db.users.insertMany([
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "5347786",
			email: "jane@janedoe.com"
		},
		courses: ["CSS", "Javascript", "Python"],
		department: "HR"

	},
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "5347786",
			email: "stephen@stephenhawking.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "HR"

	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "5347786",
			email: "neil@neilarmstrong.com"
		},
		courses: ["React", "Lavarel", "SASS"],
		department: "HR"

	}
])

// 2.

db.users.find({
	$or: [
		{
			firstName: {
		$regex: "s",
		$options: "$i"
		}
		},

		{
			lastName: {
		$regex: "d",
		$options: "$i"
		}
		}
	]
},
		{
			firstName: 1,
			lastName: 1,
			_id: 0
		}
)

/*
Find users who are from the HR department and their age is greater
than or equal to 70.
a. Use the $and operator
*/



// 3. Find users who are from the HR department and their age is greater
//than or equal to 70.

db.users.find({
	$and: [
		{
			age: { $gte: 70}
		},
		{
			department: "HR"
		}
	]
})

// 4. Find users with the letter e in their first name and has an age of less
//than or equal to 30.

db.users.find({
	$and: [
		{
			firstName: {$regex: "e"}
		},
		{
			age: { $lte: 30}
		}
	]
})